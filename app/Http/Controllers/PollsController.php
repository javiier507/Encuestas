<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;
use App\Poll;
use App\Option;
use DB;

class PollsController extends Controller
{
    public function trending()
    {
    	$polls = DB::table('options')
            ->join('polls', 'options.poll_id', '=', 'polls.id')
            ->select('polls.id', 'polls.name', DB::raw('SUM(options.votes) as votes'))
            ->groupBy('polls.name')
            ->orderBy('votes', 'desc')
            ->take(3)
            ->get();
        
        return $polls;
    }

    public function news()
    {
    	$polls = Poll::orderBy('id', 'desc')->take(3)->get();
    	return $polls;
    }

    public function categories()
    {
        return Category::all();
    }

    public function byCategory($id)
    {
    	$polls = Poll::where('categorie_id', $id)->orderBy('id', 'desc')->take(9)->get();
    	return $polls;
    }

    public function show($id)
    {
    	$poll = DB::table('polls')
            ->join('categories', 'polls.categorie_id', '=', 'categories.id')
            ->select('polls.name', 'polls.description', 'categories.name as categorie')
            ->where('categories.id', '=', $id)            
            ->get();

        $options = Option::where('poll_id', $id)
            ->select('id', 'name', 'votes')
            ->orderBy('votes', 'desc')
            ->get();

        $poll[0]->options = $options;

    	return response()->json($poll);
    }    

    public function vote($id)
    {
    	$option = Option::find($id);
		$option->votes = (int)($option->votes + 1);
		$option->save();

        return response()->json(['id' => $id]);
    }
}
