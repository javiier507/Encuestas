<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api', 'middleware' => 'cors'], function()
{
	Route::get('/', function()
	{
		return 'api encuestas pty !';
	});

	Route::get('trending', 'PollsController@trending');
	Route::get('news', 'PollsController@news');
	Route::get('categories', 'PollsController@categories');
	Route::get('category/{id}', 'PollsController@byCategory');

	Route::get('poll/{id}', 'PollsController@show');
	Route::get('vote/{id}', 'PollsController@vote');
});