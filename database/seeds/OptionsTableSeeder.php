<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            [
            	'name' => 'investigar',
            	'votes' => 456,
            	'poll_id' => 1
            ],
            [
            	'name' => 'proteger a los trabajadores',
            	'votes' => 22,
            	'poll_id' => 1
            ],
            [
            	'name' => 'reforzar la seguridad vial',
            	'votes' => 345,
            	'poll_id' => 2
            ],
            [
            	'name' => 'habilitar vias para ciclistas',
            	'votes' => 45,
            	'poll_id' => 2
            ],
            [
            	'name' => 'si es amenza',
            	'votes' => 67,
            	'poll_id' => 3
            ],
            [
            	'name' => 'no es amenza',
            	'votes' => 3,
            	'poll_id' => 3
            ],            
            [
            	'name' => 'ha funcionado',
            	'votes' => 56,
            	'poll_id' => 4
            ],
            [
            	'name' => 'requiere más tiempo',
            	'votes' => 211,
            	'poll_id' => 4
            ],
            [
            	'name' => 'si hay censura',
            	'votes' => 45,
            	'poll_id' => 5
            ],
            [
            	'name' => 'es para protección',
            	'votes' => 42,
            	'poll_id' => 5
            ]
        ]);
    }
}
