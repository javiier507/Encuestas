<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
            	'name' => 'política',
            	'description' => 'descripcion ###',
            ],
            [
            	'name' => 'seguridad',
            	'description' => 'descripcion ###',
            ],
            [
            	'name' => 'salud',
            	'description' => 'descripcion ###',
            ],
            [
            	'name' => 'educación',
            	'description' => 'descripcion ###',
            ],
            [
            	'name' => 'generales',
            	'description' => 'descripcion ###',
            ]
        ]);
    }
}
