<?php

use Illuminate\Database\Seeder;

class PollsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('polls')->insert([
            [
            	'name' => 'Odebrecht',
            	'description' => 'El presidente Juan Carlos Varela se abstuvo de 
            					dar una opinión sobre el hecho de que André Rabello, 
            					director de la filial de constructora Odebrecht en Panamá',
            	'categorie_id' => 1
            ],
            [
            	'name' => 'ciclistas',
            	'description' => 'En 2015 murieron 27 ciclistas por accidentes de tránsito',
            	'categorie_id' => 2
            ],
            [
            	'name' => 'virus zika',
            	'description' => 'El virus zika deja de ser emergencia sanitaria mundial: OMS',
            	'categorie_id' => 3
            ],
            [
            	'name' => 'transformación curricular',
            	'description' => 'El nuevo modelo fue implementado por la pasada 
            					administración del Ministerio de Educación e implicó,
            					entre otras cosas, la reducción de bachilleratos.',
            	'categorie_id' => 4
            ],
            [
            	'name' => 'Rusia bloquea la red social LinkedIn',
            	'description' => 'El Kremlin negó las acusaciones de censura en el caso de LinkedIn.',
            	'categorie_id' => 5
            ]
        ]);
    }
}
