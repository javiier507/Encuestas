<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            	'name' => 'carlos peñalba',
            	'email' => 'dev@gmail.com',
            	'password' => bcrypt('secret'),
            ],
            [
            	'name' => 'jean guerrel',
            	'email' => 'canela@gmail.com',
            	'password' => bcrypt('secret'),
            ]
        ]);
    }
}
